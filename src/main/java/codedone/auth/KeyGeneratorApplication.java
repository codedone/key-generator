package codedone.auth;

import java.security.*;
import java.util.Base64;

public class KeyGeneratorApplication {

	public static void main(String... args) throws NoSuchAlgorithmException {
		KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
		keyGenerator.initialize(1024);
		KeyPair kp = keyGenerator.genKeyPair();
		PublicKey publicKey = kp.getPublic();
		PrivateKey privateKey = kp.getPrivate();

		System.out.println("Public Key:");
		System.out.println(convertToStringKey(publicKey));

		System.out.println("Private Key:");
		System.out.println(convertToStringKey(privateKey));

	}

	private static String convertToStringKey(Key key){
		String encodingKey = Base64.getEncoder().encodeToString(key.getEncoded());
		String text = key instanceof PublicKey ? "PUBLIC" :  "PRIVATE";
		return "-----BEGIN " +
				text +
				" KEY-----\n" +
				encodingKey +
				"\n-----END " +
				text +
				" KEY-----";
	}

}
